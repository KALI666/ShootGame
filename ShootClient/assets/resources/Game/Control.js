
var Direction = {
    Z : cc.v2(0,0),
    RU  : cc.v2(0.7,0.7),
    R : cc.v2(1,0),
    U : cc.v2(0,1),
    LU : cc.v2(-0.7,0.7),
    L : cc.v2(-1,0),
    LD  : cc.v2(-0.7,-0.7),
    D  : cc.v2(0,-1),
    RD  : cc.v2(0.7,-0.7)
}

cc.Class({
    extends: cc.Component,

    properties: {
        yaogan : cc.Node,
        btnCircle :cc.Node,
        leftShootBtn : cc.Button,
        rightShootBtn : cc.Button
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        
        this.lastDrection = Direction.Z;
        this.lastPoint = cc.Vec2.ZERO
        this._touchEnable = false;
        this._keyCodes = [];
        this.leftShootBtn.interactable = true;
        this.rightShootBtn.interactable = false;
    },

    setTouchEnabled(e){
        if(this._touchEnable == e){
            return;
        }
        this._touchEnable = e;
        if(e){
            this.yaogan.on(cc.Node.EventType.TOUCH_START,this.onTouch,this);
            this.yaogan.on(cc.Node.EventType.TOUCH_MOVE,this.onTouch,this);
            this.yaogan.on(cc.Node.EventType.TOUCH_END,this.onTouch,this);
            this.yaogan.on(cc.Node.EventType.TOUCH_CANCEL,this.onTouch,this);
            cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onDeviceMotionEvent, this);
            cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onDeviceMotionEvent, this);
        } else {
            this.yaogan.off(cc.Node.EventType.TOUCH_START);
            this.yaogan.off(cc.Node.EventType.TOUCH_MOVE,);
            this.yaogan.off(cc.Node.EventType.TOUCH_END);
            this.yaogan.off(cc.Node.EventType.TOUCH_CANCEL);
            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN);
            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP);
        }
    },

    onDeviceMotionEvent(e){
        // cc.log('e:',e.keyCode)
        if([87,83,65,68,74,75].findIndex(a=>{return a == e.keyCode}) < 0){
            return
        }

        if(e.type == cc.SystemEvent.EventType.KEY_DOWN){
            if(e.keyCode == 74){
                this.onClickShootAngle(null,'left')
                return;
            } else if(e.keyCode == 75){ 
                this.onClickShootAngle(null,'right')
                return;
            }
            
        }

        // cc.log('onDeviceMotionEvent:',e.keyCode)
        var idx = this._keyCodes.findIndex(item=>{return item == e.keyCode})
        if(e.type == cc.SystemEvent.EventType.KEY_DOWN){
            if(idx < 0){
                this._keyCodes.push(e.keyCode);
            } else {
                return
            }
        } else if(idx >= 0){
            this._keyCodes.splice(idx,1);
        }
        var dir = cc.v2(0,0)
        if(this._keyCodes.length){
            dir = this.getDiectionByCode(this._keyCodes[this._keyCodes.length-1])
        }
        if(this._keyCodes.length > 1){
            var d2 = this.getDiectionByCode(this._keyCodes[this._keyCodes.length-2])
            if(dir.x * d2.x < 0 || dir.y * d2.y < 0){
                dir = d2;
            } else {
                dir = cc.v2(dir.x||d2.x,dir.y||d2.y)
                if(dir.x !=0 && dir.y != 0){
                    dir.x *= 0.7
                    dir.y*= 0.7
                }
            }
        }
        // cc.log('direction:',this._keyCodes)
        if(!this.lastDrection.equals(dir)){
            this.lastDrection = dir
            this.emitDir(dir);
        }
    },

    emitDir(d){
        this.node.emit('control',d);
    },
    emitBtn(d){
        this.node.emit('onbtn',d);
    },

    onTouch(event){
        var point = this.yaogan.convertToNodeSpaceAR(event.getLocation())
        // cc.log('point:',point);
        if(event.type == cc.Node.EventType.TOUCH_START){
            this.lastDrection = Direction.Z
            this.lastPoint = cc.Vec2.ZERO
        } else if(event.type == cc.Node.EventType.TOUCH_END || event.type == cc.Node.EventType.TOUCH_CANCEL){
            this.btnCircle.x = 0;
            this.btnCircle.y = 0;
            this.emitDir(Direction.Z)
            return;
        }

        var len = Math.min(point.mag(),100);
        var r = global.Common.getAngle(cc.v2(0,0),point)
        var x = Math.cos(r) * len
        var y = Math.sin(r) * len;
        this.btnCircle.x = x;
        this.btnCircle.y = y;

        var d = this.getDiection(r)

        if(!d.equals(this.lastDrection)){
            // cc.log('d: ',r1*180/Math.PI,d)
            this.lastDrection = d;
            this.emitDir(d)
        }
    },
    onClickShootAngle(e,d){
        // this.leftShootBtn.interactable = true;
        // this.rightShootBtn.interactable = true;
        this.emitBtn(d);
        // e.target.getComponent(cc.Button).interactable = false;
    },
    setShootBtn(d){
        this.leftShootBtn.interactable = d != 'left';
        this.rightShootBtn.interactable = d != 'right';
    },
    getDiectionByCode(code){
        switch(code){
            case 87:return Direction.U;
            case 83:return Direction.D;
            case 65:return Direction.L; 
            case 68:return Direction.R;
        }
    },
    getDiection(r){
        var a = r*180/Math.PI 
        if(a < 22.5){
            return Direction.R
        } else if(a < 67.5){
            return Direction.RU
        } else if(a < 112.5){
            return Direction.U
        }else if(a < 157.5){
            return Direction.LU
        }else if(a < 202.5){
            return Direction.L
        }else if(a < 247.5){
            return Direction.LD
        }else if(a < 292.5){
            return Direction.D
        }else if(a < 337.5){
            return Direction.RD
        }else{
            return Direction.R
        }
    },
    gameUpdate(dt){
    }

    // update (dt) {},
});
