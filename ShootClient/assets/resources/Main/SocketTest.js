
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        editBox : cc.EditBox
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        global.Socket.on('loginSuccess',function(err,res){
            cc.log('loginSuccess:',res);
        })
    },

    onClick(e,d){
        if(d == 'connect'){
            global.Socket.connect('127.0.0.1',"8001");
        } else if(d == 'close'){

        } else if(d == 'send'){
            var text = this.editBox.string
            cc.log('text:',text);
        }
    }

    // update (dt) {},
});
